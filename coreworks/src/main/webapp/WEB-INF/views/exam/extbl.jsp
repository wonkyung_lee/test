<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/submenu.jsp" />
			
			 <div id="scroll_area" class="inner_rt">
              <!-- 메인 컨텐츠 영역 시작! -->
                  <div class="tbl_top_wrap">
                      <div class="menu_tit"><h2>내 결재완료</h2></div>
                      <!-- 테이블 위 컨텐츠 시작 -->
                      여기에 채우기
                      <!-- 테이블 위 컨텐츠 끝 -->
                  </div>
                  <!-- 전자결재 테이블 시작 -->
                  <div class="tbl_common eas_wrap">
                      <div class="tbl_wrap">
                          <table class="tbl_ctn">
                              <tr>
                                  <th>제목1</th>
                                  <th>제목2</th>
                                  <th>제목3</th>
                                  <th>제목4</th>
                                  <th>제목5</th>
                              </tr>
                              <tr>
                                  <td><a href="#">내용1</a></td>
                                  <td><a href="#">내용2</a></td>
                                  <td><a href="#">내용3</a></td>
                                  <td><a href="#">내용4</a></td>
                                  <td><a href="#">내용5</a></td>
                              </tr>
                              <tr>
                                  <td><a href="#">내용1</a></td>
                                  <td><a href="#">내용2</a></td>
                                  <td><a href="#">내용3</a></td>
                                  <td><a href="#">내용4</a></td>
                                  <td><a href="#">내용5</a></td>
                              </tr>
                          </table>
                      </div>
                  </div>
                  <!-- 전자결재 테이블 끝 -->
                  <!-- 페이저 시작 -->
                  <div class="pager_wrap">
                      <ul class="pager_cnt clearfix">
                      <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                      <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">2</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                      <li class="pager_com pager_num on"><a href="javascrpt: void(0);">4</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">5</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                      <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                      <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                      <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                      </ul>
                  </div>
                  <!-- 페이저 끝 -->
              <!-- 메인 컨텐츠 영역 끝! -->
              </div><!-- inner_rt end -->



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(0).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
    });
</script>
<jsp:include page="../chat/chat.jsp"/>
</body>
</html>