<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:include page="./header.jsp" />
<jsp:include page="./submenu.jsp" />
 <link rel="stylesheet" href="${ contextPath }/resources/asset/jqx.base.css" type="text/css" />
 <script type="text/javascript" src="${ contextPath }/resources/asset/jqxcore.js"></script>
 <script type="text/javascript" src="${ contextPath }/resources/asset/jqxbuttons.js"></script>
 <script type="text/javascript" src="${ contextPath }/resources/asset/jqxpanel.js"></script>
 <script type="text/javascript" src="${ contextPath }/resources/asset/jqxtree.js"></script>
 <script type="text/javascript" src="${ contextPath }/resources/asset/jqxscrollbar.js"></script>
 <style>
 
/* tree api */
.jqx-widget-content{font-size: 16px;}
.jqx-tree-item{padding: 6px;}
.jqx-widget-content{color: #fff;background-color: transparent;border: none;}
.jqx-fill-state-pressed{background-color: transparent;border-color: transparent;color: #CB7999;text-decoration: underline;}
.jqx-fill-state-pressed span{text-decoration: underline;}
.jqx-fill-state-hover{background-color: transparent;border-color: transparent;color: #CB7999;text-decoration: underline;box-sizing: content-box;}
.jqx-fill-state-hover span{text-decoration: underline;}
.jqx-item{vertical-align: middle;}
.tree_img{margin: 0 4px 0 0;float: left;width: 20px;}
.tree_tit{float: left;}
 </style>
<%--  
 <script type="text/javascript" src="${ contextPath }/resources/asset/jqxbuttons.js"></script>
 <script type="text/javascript" src="${ contextPath }/resources/asset/jqxscrollbar.js"></script>
 <script type="text/javascript" src="${ contextPath }/resources/asset/jqxpanel.js"></script>
 <script type="text/javascript" src="${ contextPath }/resources/asset/jqxexpander.js"></script> --%>

<script type="text/javascript">
$(document).ready(function () {
    // Create jqxTree
    $('#jqxTree').jqxTree();
});
</script>
 <main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
              
		    <div id='jqxTree'>
		        <ul>
		            <li id='home'>Home</li>
		            <li item-expanded='true'><div class="clearfix"><img src="${contextPath}/resources/images/w_folder.svg" class="tree_img"><span class="tree_tit">메뉴</span></div>
		                        <ul>
		                            <li><a href="#" class="clearfix"><img src="${contextPath}/resources/images/w_folder.svg" class="tree_img"><span class="tree_tit">메뉴</span></a></li>
		                            <li>Financial services</li>
		                            <li>Government</li>
		                            <li>Manufacturing</li>
		                            <li>Solutions
		                                <ul>
		                                    <li>Consumer photo and video</li>
		                                    <li>Mobile</li>
		                                    <li>Rich Internet applications</li>
		                                    <li>Technical communication</li>
		                                    <li>Training and eLearning</li>
		                                    <li>Web conferencing</li>
		                                </ul>
		                            </li>
		                            <li>All industries and solutions</li>
		                        </ul>
		            </li>
		            <li>Products
		                        <ul>
		                            <li>PC products</li>
		                            <li>Mobile products</li>
		                            <li>All products</li>
		                        </ul>
		            </li>
		        </ul>
		    </div>
            </div>
        </div><!-- inner_lt end -->