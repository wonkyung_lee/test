<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="./header.jsp" />
<jsp:include page="./submenu.jsp" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>

<style>
.tree_wrap{padding-top: 20px;}
.jstree{color: #fff;}
.jstree-default .jstree-hovered{color: #CB7999;font-weight: bold;text-decoration: underline;background-color: transparent;border-color: transparent;}
.jstree-default .jstree-node{line-height: 30px;}
.jstree-default .jstree-clicked{color: #CB7999;font-weight: bold;text-decoration: underline;background-color: transparent;border-color: transparent;}
.jstree-default .jstree-node, .jstree-default .jstree-icon{background-image: url(${contextPath}/resources/images/treeicon.png);}
.jstree-default .jstree-node{background-position: -303px -1px; background-repeat: repeat-y;}
</style>
 <main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap tree_wrap">
              <div id="jstree">
			    <ul>
			      <li>메뉴1
			        <ul>
			          <li id="child_node_1">메뉴 1_1</li>
			          <li>메뉴 1_2</li>
			        </ul>
			      </li>
			      <li>메뉴2
			        <ul>
			          <li>메뉴 1_1</li>
			          <li>메뉴 1_2</li>
			        </ul>
			      </li>
			    </ul>
			  </div>
		      <script type="text/javascript">
				 $(function () {
				    $("#jstree").jstree({
				    	"themes" : {
							"theme" : [ "classic" ]
						}
				    });
				    
				    // 7 bind to events triggered on the tree
				    $('#jstree').on("changed.jstree", function (e, data) {
				      console.log(data.selected);
				    });
				    // 8 interact with the tree - either way is OK
				    $('button').on('click', function () {
				      $('#jstree').jstree(true).select_node('child_node_1');
				      $('#jstree').jstree('select_node', 'child_node_1');
				      $.jstree.reference('#jstree').select_node('child_node_1');
				    });
				  });
			</script>
            </div>
        </div><!-- inner_lt end -->
        
        