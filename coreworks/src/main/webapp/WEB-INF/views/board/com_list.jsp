<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
	파일설명 : 전체 공지게시판
--%>

<jsp:include page="../inc/board_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>전체 공지사항</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <form action="" method="get">
	                    <div class="main_cnt">
	                        <select name="" id="">
	                            <option value="">제목</option>
	                            <option value="">작성자</option>
	                            <option value="">내용</option>
	                        </select>
	                        <input type="text" name="" id="">
	                        <!-- 색 있는 버튼 -->
	                        <button class="btn_solid_main" type="submit">검색</button>
	                    </div>
                    </form>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                             	<colgroup>
                                    <col style="width: 8%;">
                                    <col style="width: 84%;">
                                    <col style="width: 8%;">
                                </colgroup>
	                            <c:forEach var="i" begin="0" end="14" step="1">
	                                <tr>
							            <td><div>필독</div></td>
							            <td>
							            	<div>
								                <div>인장관리규정 신설 및 시행 안내</div>
								                <div>
									                <span>이원경</span>
									                <span>조회수 123</span>
									                <span>댓글 3</span>
								                </div>
							                </div>
							            </td>
							            <td><div>05:54 PM</div></td>
	                                </tr>
	                            </c:forEach>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
                        <li class="pager_com spager_num"><a href="javascrpt: void(0);">5</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>




<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
        
        // text-align-last center로 변경
        // $('.tbl_common .tbl_basic .tbl_wrap').css('text-align-last', 'center');
        // $('.tbl_common .tbl_basic .tbl_wrap').removeAttr('style');
    });
</script>
</body>
</html>
