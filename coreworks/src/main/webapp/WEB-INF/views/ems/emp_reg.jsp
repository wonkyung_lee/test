<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/ems_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">

            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>사원 등록</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                     
                    <div class="main_cnt_list clearfix">
                  
               <div id="logoImgArea">
                  <img id="titleImg" width="120px" height="120px">
                  <a href="#" class="button btn_main" style="margin:10 0">사진 등록</a>
               </div>
                    
                    
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사번</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">아이디</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">비밀번호</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">이름</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">부서</div>
                            <div class="main_cnt_desc">
	                            <select name="" id="">
	                            	<option value="">부서</option>
	                            	<option value="">부서</option>
	                        	</select>
	                      	</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">직급</div>
                            <div class="main_cnt_tit">
                            	<select name="" id="">
	                            	<option value="">직급</option>
	                            	<option value="">직급</option>
	                        	</select>
                            </div>
                            <div class="main_cnt_tit">직책</div>
                            <div class="main_cnt_desc">
                            	<select name="" id="">
	                            	<option value="">직책</option>
	                            	<option value="">직책</option>
	                        	</select>
                            </div>
                        </div>                      
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">내선전화</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                            
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">휴대전화</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="phone"> - <input type="text" name="" id="phone"> - <input type="text" name="" id="phone"></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">생일</div>
                            <div class="main_cnt_desc"><input type="date" name="" id=""></div>
                        </div>                
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">주소</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""><a href="#" class="button btn_pink" style="margin: 0 10;">주소 검색</a></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">성별</div>
                            <div class="main_cnt_tit">
                            	<input type="radio" name="" id=""><label>남</label>
                           	</div>
                           	<div class="main_cnt_tit">
                            	<input type="radio" name="" id=""><label>여</label>
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">팩스번호</div>
                            <div class="main_cnt_desc"><input type="text" name="" id=""></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">입사일</div>
                            <div class="main_cnt_desc"><input type="date" name="" id=""></div>
                        </div>
                        <div class="insert_btn">
                        	<button class="btn_main">취소</button>
                			<button class="btn_main">확인</button>
                   		</div>                                              
                    </div>
                    <!-- ** 코드 작성부 끝 -->
                    <!-- <div style="float:right;">
                        <button class="btn_pink">퇴사처리</button>
                		<button class="btn_main">수정하기</button>
                    </div> -->              
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(6).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on");
    });
</script>
</body>
</html>