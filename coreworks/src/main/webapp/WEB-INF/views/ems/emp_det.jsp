<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/ems_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">

            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>상세보기</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                     
                    <div class="main_cnt_list clearfix">
                  
               <div id="logoImgArea">
                  <img id="titleImg" width="120px" height="120px">
               </div>
                    
                    
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사번</div>
                            <div class="main_cnt_desc">9999</div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">아이디</div>
                            <div class="main_cnt_desc">bap99</div>
                        </div> 
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">이름</div>
                            <div class="main_cnt_desc">최원준</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">부서</div>
                            <div class="main_cnt_desc">개발부서</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">직급/직책</div>
                            <div class="main_cnt_desc">팀장/대리</div>
                        </div>                      
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">내선전화</div>
                            <div class="main_cnt_desc">02-709-9999</div>
                            
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">휴대전화</div>
                            <div class="main_cnt_desc">010-9999-9999</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">생일</div>
                            <div class="main_cnt_desc">96/09/09</div>
                        </div>                
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">주소</div>
                            <div class="main_cnt_desc">경기도 파주시 구구동 구구마을 구구아파트 9동 9호</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">성별</div>
                            <div class="main_cnt_tit">남</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">팩스번호</div>
                            <div class="main_cnt_desc">02-709-9999</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">입사일</div>
                            <div class="main_cnt_desc">17/09/09</div>
                        </div>
                        <div class="insert_btn">
                        	<button class="btn_pink" onclick="retire();">퇴사처리</button>
                			<button class="btn_main">수정하기</button>
                   		</div>                                              
                    </div>
                    <!-- ** 코드 작성부 끝 -->
                    <!-- <div style="float:right;">
                        <button class="btn_pink">퇴사처리</button>
                		<button class="btn_main">수정하기</button>
                    </div> -->              
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(6).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on");

    });
    
    function retire() {
    	alert("퇴사처리 하시겠습니까?");
    }
</script>
</body>
</html>