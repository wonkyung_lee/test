<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/eas_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<!-- date picker css -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div id="scroll_area" class="inner_rt">
	<!-- 메인 컨텐츠 영역 시작! -->
	<div class="tbl_top_wrap">
		<div class="menu_tit">
			<h2>조회</h2>
		</div>
		<!-- 탭 영역 위 컨텐츠 시작 -->
			<a href="#eas_record_pop" rel="modal:open" class="button btn_main">이력보기</a>
			<button class="btn_main">PC저장</button>
			<button class="btn_main">인쇄</button>
		<!-- 탭 영역 위 컨텐츠 끝 -->	
	</div>
	<!-- 탭 영역 시작 -->
	<div class="main_ctn">
    	<ul class="tabs">
			<li class="tab-link current" data-tab="tab-1">결재정보</li>
			<li class="tab-link" data-tab="tab-2">본문</li>							
		</ul>
		<!-- 결재정보 탭 클릭 시 -->
        <div id="tab-1" class="tab-content current">
		<!-- 테이블 위 컨텐츠 시작 -->
        <table class="eas_information top_eas_information">
        	<tr>
        		<td class="eas_information_title">문서정보</td>
        		<td><input type="checkbox" id="eas_argent"><label for="eas_argent">긴급</label></td>
        	</tr>
        	<tr>
        		<td>제목</td>
        		<td>분상과석 600톤 구매의 건</td>
        		<td>문서번호</td>
        		<td>2020-01</td>
        	</tr>
        	<tr class="eas_last_tr">
        		<td>열람제한</td>
        		<td>
        			<label for="limit"><input type="radio" class="new_eas_radio" id="limit" name="view_limit" checked>제한종료일</label>
        			<input type="text" name="" id="datepicker" class="datepicker limit_date">
        			<label for="nolimit"><input type="radio" class="new_eas_radio" id="nolimit" name="view_limit">영구</label>
        		</td>
        		<td>보안설정</td>
        		<td>
        			<label for="security_y"><input type="radio" class="new_eas_radio" id="security_y" name="security">사용</label>
        			<label for="security_n"><input type="radio" class="new_eas_radio" id="security_n" name="security" checked>미사용</label>
        		</td>
        	</tr>
        </table>
			<!-- 보안설정 사용 클릭 시 나오는 부분, 추후 바꿀 예정 -->
			<div>
				<div class="security_div">
					<div class="security_div2 gg">
						<input type="radio" class="new_eas_radio" id="security_radio"
							name="security_radio" checked> <input type="checkbox"
							id="security_1" name="security_check"><label
							for="security_1">상급자</label> <input type="checkbox"
							id="security_2" name="security_check"><label
							for="security_2">동급자</label> <input type="checkbox"
							id="security_3" name="security_check"><label
							for="security_3">동일부서</label>
					</div>
					<div class="security_div2">
						<input type="radio" class="new_eas_radio" id="security_radio"
							name="security_radio"> <input type="text"
							class="security_text" placeholder="직접 선택해주세요.">
					</div>
				</div>
			</div>
			<!-- 보안설정 사용 클릭 시 나오는 부분 끝 -->
			<table class="eas_line_information">
        	<tr>
        		<td class="eas_information_title">결재경로</td>
        	</tr>
		</table>
			<!-- 결재 경로 -->
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
					<table class="tbl_ctn">
						<tr class="tbl_main_tit">
							<th>순번</th>
							<th>처리방법</th>
							<th>직책(직위)</th>
							<th>처리자</th>
							<th>처리 상태</th>
							<th>처리 일시</th>
							<th>본문 버전</th>
						</tr>
						<tr>
							<td class="tit">4</td>
							<td>결재</td>
							<td>대표이사</td>
							<td>김진호</td>
							<td>대기</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="tit">3</td>
							<td>결재</td>
							<td>CFO</td>
							<td>윤재영</td>
							<td>대기</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="tit">2</td>
							<td>결재</td>
							<td>파트장</td>
							<td>이원경</td>
							<td>진행</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="tit">1</td>
							<td>확인</td>
							<td>팀원</td>
							<td>조문정</td>
							<td>완료</td>
							<td>20/01/06 10:51AM</td>
							<td>0</td>
						</tr>
					</table>
				</div>
			</div>
			<!-- 결재 경로 끝 -->
			
			<!-- 수신자 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">수신자</td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl2">
				<div class="tbl_wrap">
					<table class="tbl_ctn eas_sender_tbl">
						 <colgroup>
						    <col style="width: 25%;">
						    <col style="width: 25%;">
						    <col style="width: 25%;">
						    <col style="width: 25%;">
					    </colgroup>
						<tr>
							<td class="tbl_main_tit sender_border">수신자</th>
							<td class="sender_border">양승현</td>
							<td class="tbl_main_tit sender_border">발신 명의</td>
							<td>구매팀장</td>
						</tr>						
					</table>
										
				</div>				
			</div>
			<!-- 수신자 끝 -->
			
			<!-- 공람자 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">공람자</td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl2">
				<div class="tbl_wrap">
					<table class="tbl_ctn eas_sender_tbl">
						 <colgroup>
						    <col style="width: 25%;">
						    <col style="width: 25%;">
						    <col style="width: 25%;">
						    <col style="width: 25%;">
					    </colgroup>
						<tr>
							<td class="tbl_main_tit sender_border">공람자</th>
							<td class="sender_border">남윤진</td>
							<td class="tbl_main_tit sender_border">발신 명의</td>
							<td>재무파트/대리</td>
						</tr>
						<tr>
							<td class="tbl_main_tit sender_border">공람자</th>
							<td class="sender_border">박상준</td>
							<td class="tbl_main_tit sender_border">발신 명의</td>
							<td>자금파트/매니저</td>
						</tr>						
					</table>
										
				</div>				
			</div>
			<!-- 공람자 끝 -->
			
			<!-- 관련문서 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">관련 문서</td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
					<table class="tbl_ctn">						
						<tr class="tbl_main_tit">
							<th>문서번호</th>
							<th>제목</th>
							<th>기안자</th>
							<th>기안 부서</th>
							<th>기안 일시</th>
						</tr>
						<tr class="non_doc">
							<td class="tit">2020-10</td>
							<td>뱁새 사료 대금</td>
							<td>조문정</td>
							<td>구매파트</td>
							<td>19/12/31</td>
						</tr>
					</table>
				</div>
			</div>
			<!-- 관련 문서 끝 -->
			
			<!-- 첨부파일 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">첨부 파일</td>
        		<td>
	        		<div class="file_area"></div>
        		</td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
					<table class="tbl_ctn fileTbl">
						<colgroup>
							<col style="width: 70%;">
							<col style="width: 13%;">
							<col style="width: 7%;">
						</colgroup>
						<tr class="tbl_main_tit">
							<th>파일명</th>
							<th>크기</th>					
						</tr>						
						<tr class="non_file">
							<td class="tit"><p id="fileName" name="fileName">첨부된 파일이 없습니다.</p></td>
							<td><a href="#"></a></td>							
						</tr>
						
						
					</table>
					
				</div>
			</div>
			<!-- 첨부파일 끝 -->
		</div>
		<!-- 결재정보 탭 끝 -->
		
		<!-- 본문 탭 시작 -->
		<div id="tab-2" class="tab-content">
		<!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap eas_tbl_doc">
                            <table class="tbl_ctn">
                                <colgroup>
                                    <col style="width: 80px;">
                                    <col style="width: *;">
                                    <col style="width: 80px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                </colgroup>
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">문서번호</td>
                                    <td rowspan="2" class="doc_con">20200115-몰라-몰라</td>
                                    <td rowspan="4" class="doc_con small doc_tit">결재</td>
                                    <td class="doc_con gray doc_tit">팀장</td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>                                 
                                </tr>
                               	<tr class="eas_tit">                                 
                                    <td rowspan="3" class="doc_con">이원경</td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                </tr>                             
                                <tr class="eas_tit" >
                                    <td rowspan="2" class="doc_con doc_tit">작성일자</td>
                                    <td rowspan="2" class="doc_con">20/01/15</td>                         	                                                                   
                                </tr>
                                <tr class="eas_tit"></tr>                                
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">작성부서</td>
                                    <td rowspan="2" class="doc_con">구매파트</td>
                                    <td rowspan="4" class="doc_con small doc_tit">합의</td>
                                    <td class="doc_con gray doc_tit">합의자</td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>                                 
                                </tr>
                                <tr class="eas_tit">
                                    <td rowspan="3" class="doc_con border_bottom">합의자 이름</td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>                                
                                </tr>
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">작성자</td>
                                    <td rowspan="2" class="doc_con">조문정</td>                              
                                </tr>  
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit">
                                    <td class="doc_con doc_tit">수신자</td>
                                    <td colspan="7" class="doc_con">양승현</td>                                 
                                </tr>
                                <tr class="eas_tit">
                                    <td class="doc_con doc_tit">제목</td>
                                    <td colspan="7" class="doc_con">뱁새 사료 구매의 건</td>                                
                                </tr>
                                <!-- 기안 본문 작성 부분 -->
                                <tr class="eas_tit">
                                    <td rowspan="10" colspan="8" class="eas_content"></td>                              
                                </tr>
                               	<tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>  
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit"></tr>
                                <!-- 기안 본문 작성 부분 끝 -->                                                       
                            </table>
                        </div>
                    </div>
        <!-- 기본 테이블 끝 -->
		</div>
		<!-- 본문 탭 끝 -->
    </div>
    <!-- 탭 영역 끝 -->
    </div>
	<!-- 메인 컨텐츠 영역 끝! -->
</div>
<!-- inner_rt end -->
</div>
</main>
</div>

<!-- popup include -->
<div id="eas_record_pop" class="modal">
	<jsp:include page="../pop/eas_record_pop.jsp" />
</div>


<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(1).addClass("on");
        
        var path = '${ contextPath }';
	
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(2).addClass("on");

        // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
        $("#menu_area .menu_list").eq(1).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(2).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
    
    	
        (function(){
        	var idx = 0;
        	var fileNode = '<input type="file" id="">';	
        	var fileTr = '<tr><td class="tit"><p></p></td><td><a href="#"></a></td><td></td></tr>';	
        	
        	$(".upFileBtn").on("click",function(){
        		idx++;
        		$(".file_area").eq(0).append(fileNode).css("display", "none").find("input:last-child").prop("id", "upFile"+idx).trigger("click");        		
        		
        		$(document).on("change", "#upFile"+idx, function(){
	        		if(idx == 1){
	        			$(".non_file").eq(0).css({display: "none"});
	        		}
	        		$(".fileTbl").append(fileTr);
	        		$(".fileTbl").find("tr").eq(idx+1).find(".tit p").eq(0).text($(this).val().split('/').pop().split('\\').pop());
        		});
        		
        	});
        	
        })();
        
        $("#checkAll").change(function(){
			if($("#checkAll").is(":checked")){
				console.log("체크박스 체크했음!");
			}else{
				console.log("체크박스 체크 해제!");
			}
		});
    
    });
    
  //탭 전환용 스크립트
    $(document).ready(function(){
    	
    	$('ul.tabs li').click(function(){
    		var tab_id = $(this).attr('data-tab');

    		$('ul.tabs li').removeClass('current');
    		$('.tab-content').removeClass('current');

    		$(this).addClass('current');
    		$("#"+tab_id).addClass('current');
    	})

    })
    
    //보안 설정용 스크립트
    $("#security_y").click(function(){
    	$(".eas_information tr:nth-child(3) td").css("padding-bottom", "0");
    	$(".security_div").show();
    });
  	
    $("#security_n").click(function(){
    	$(".security_div").hide();
    	$(".eas_information tr:nth-child(3) td").css("padding-bottom", "20px");
    });
    
    $("#datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        nextText: '다음 달',
        prevText: '이전 달',
        dateFormat: "yy/mm/dd",
        showMonthAfterYear: true , 
        dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
    });                
   
</script>
</body>
</html>