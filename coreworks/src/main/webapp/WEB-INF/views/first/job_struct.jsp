<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:include page="../inc/cms_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
				<a href="#" class="button btn_blue save">저장</a>
				<a href="#" class="button btn_blue add" onclick="topAdd()">추가</a>
                    <div class="menu_tit"><h2>직급 관리</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
            	
		
                        
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                <tr class="tbl_main_tit">
                                    <th>직급번호</th>
                                    <th>직급명</th>
                                    <th>순서변경</th>
                                    <th>수정/삭제</th>
                                </tr>
				

				</table>
                    </div>
                     
                          
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                   
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
		
		<!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
var i = 1;
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
    	$("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on");
        
        
        
        
        

        
    });
    function topAdd() {

    	var str = "<tr><td></td><td><a href='#'><input type='text' name=" + ("'dept" + i++) +"'" + " style='text-align:center;'></input></a></td><td><a href='#'><button class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' onclick='moveDown(this)'>▼</button></a></td><td><button class='btn_main'>수정</button><button class='btn_white'>삭제</button></td></tr>";
    	$(str).appendTo($(".tbl_ctn"));
    }
    
   /*  function subAdd(val) {
    	var str2= "<tr><td><a href='#'>└</a></td><td><a href='#'><input type='text' style='text-align:center;'></input></a></td><td><a href='#'><button class='btn_main_up'>▲</button><button class='btn_main_down'>▼</button></a></td><td><button class='btn_main'>수정</button><button class='btn_white'>삭제</button></td></tr>"
    	//val.parents()
    	console.log(val);
    } */
    $('.table1 button:even').bind('click', function(){ moveUp(this) });
    $('.table1 button:odd').bind('click', function(){ moveDown(this) });
    
    function moveUp(el){
		var $tr = $(el).closest('tr'); // 클릭한 버튼이 속한 tr 요소
    	var idx = $tr.index();
			
		if(idx > 1) {
			
		$tr.prev().before($tr); // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
		}
	
	
	}
    
    function moveDown(el){
		var $tr = $(el).closest('tr'); // 클릭한 버튼이 속한 tr 요소
		
		$tr.next().after($tr); // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
	}
    
    
</script>
</body>
</html>