<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wk.css">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/common.css">
</head>
<body>
	<div class="loginbox">
		<div class="loginlogo">
			<img src="${ contextPath }/resources/images/logo_3.png" alt="회사로고" />
		</div>
		<div class="loginform">
			<form action="" method="post">
				<div class="loginid">
					<input type="text" name="userId" id="userId" placeholder="  계정" />
				</div>
				<div class="loginpwd">
					<input type="password" name="userPwd" id="userPwd" placeholder="  비밀번호" />
				</div>
				<button class="btn_solid" id="loginbtn">로그인</button>
			</form>
		</div>
		<div class="savelogin">
			<input type="checkbox" name="saveid" id="saveid" value="계정 저장">
                     <label for="saveid">계정저장</label>
		</div>
	</div>
	<div class="logincopyright">
		@ copyright 2020 COREWORKS All right reserved
	</div>
</body>
</html>